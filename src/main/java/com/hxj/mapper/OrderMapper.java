package com.hxj.mapper;

import com.hxj.pojo.Orderx;
import com.hxj.pojo.Orderitems;

import java.util.List;

/**
 * @Date: 2021/1/20
 * @Author: 黄先军
 * @Description:
 */
public interface OrderMapper {
    /**
     * 插入新的order数据,返回的值是新插入的order的id
     * @param orderx
     * @return
     */
    Integer insert(Orderx orderx);
    /**
     * 批量插入orderitem数据
     * @param orderitemsList
     * @return
     */
    Integer insertList(List<Orderitems> orderitemsList);
    /**
     * 通过id查询order数据，这个order数据里面包含了car的集合
     * @param userid
     * @return
     */
    List<Orderx> selectByUserid(Integer userid);


}
