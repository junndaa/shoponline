package com.hxj.mapper;

import com.hxj.pojo.Car;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Date: 2021/1/19
 * @Author: 黄先军
 * @Description:
 */
public interface CarMapper {
    /**
     * 查询购物车的信息
     * @return List<User>
     */
    List<Car> selectAll();

    /**
     * 往car表里面添加一行car数据，这个时候book里面的库存不需要减一
     * @param car
     * @return 返回的是当前插入car表里面的id
     */
    Integer addCar(Car car);

    /**
     * 查询需要加一的car表里面的数据
     * @param userid
     * @param bookname
     * @return 返回商品数量
     */
    Integer findOrdernumberByuseridAndBookname(Integer userid,String bookname);

    /**
     * 更新car表里面的ordernumber数据
     * @param ordernumber
     * @param userid
     * @param bookname
     * @return
     */
    Integer updateCarOrdernumber(Integer ordernumber,Integer userid,String bookname,BigDecimal totalprice);

    /**
     *通过用户id返回购物车数据
     * @param userId
     * @return
     */
    List<Car> findCarsByUserId(Integer userId);
    /**
     *通过id删除car行数据操作
     * @param id
     * @return
     */
    Integer delete(Integer id);
    /**
     * 通过用户id清空购物车
     * @param anInt
     * @return
     */
    Integer cleanMyCar(int anInt);
}
