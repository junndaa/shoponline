package com.hxj.mapper;

import com.hxj.pojo.User;

import java.util.List;

/**
 * @Date: 2021/1/18
 * @Author: 黄先军
 * @Description:
 */
public interface UserMapper {
    /**
     * 查询所有用户的信息
     * @return List<User>
     */
    List<User> selectAll();

    /**
     * 插入新是user数据
     * @param user
     * @return
     */
    Integer insert(User user);

    /**
     * 根据用户id删除用户的信息
     * @return List<User>
     */
    Integer delete(Integer id);

    /**
     * 通过id查询user数据
     * @param id
     * @return
     */
    User selectByid(Integer id);

    /**
     * 更新user的数据
     * @param user
     * @return
     */
    Integer update(User user);
    /**
     * 通过用户名来查询用户的id，来添加商品到购物车
     * @param username
     * @return
     */
    Integer findIdByUsername(String username);

}
