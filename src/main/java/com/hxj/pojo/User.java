package com.hxj.pojo;

import lombok.*;

/**
 * @Date: 2021/1/18
 * @Author: 黄先军
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

  private Integer id;
  private String username;
  private String password;
  private String fullname;
  private String phonenumber;
  private String address;
  private String city;
  private String country;
  private String email;
  private String hobby;
  private String secret;
  private String secretanswer;
  private String userrole;
  private String isdeleted;

  /**
   * 注册用户和添加用户的构造方法
   * @param username
   * @param password
   * @param fullname
   * @param phonenumber
   * @param address
   * @param city
   * @param country
   * @param email
   * @param hobby
   * @param secret
   * @param secretanswer
   */
  public User(String username, String password, String fullname, String phonenumber, String address, String city, String country, String email, String hobby, String secret, String secretanswer) {
    this.username = username;
    this.password = password;
    this.fullname = fullname;
    this.phonenumber = phonenumber;
    this.address = address;
    this.city = city;
    this.country = country;
    this.email = email;
    this.hobby = hobby;
    this.secret = secret;
    this.secretanswer = secretanswer;
  }

  /**
   * 修改用户需要的参数
   * @param id
   * @param username
   * @param password
   * @param fullname
   * @param phonenumber
   * @param email
   * @param hobby
   * @param secret
   * @param secretanswer
   */
  public User(Integer id, String username, String password, String fullname, String phonenumber, String email, String hobby, String secret, String secretanswer) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.fullname = fullname;
    this.phonenumber = phonenumber;
    this.email = email;
    this.hobby = hobby;
    this.secret = secret;
    this.secretanswer = secretanswer;
  }
}
