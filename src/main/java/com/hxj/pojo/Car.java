package com.hxj.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;
/**
 * @Date: 2021/1/19
 * @Author: 黄先军
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Car {

  private Integer id;
  private Integer userid;
  private String bookname;
  private BigDecimal price;
  private Integer ordernumber;
  private BigDecimal totalprice;

  /**
   * 构建添加car的构造方法
   * @param userid
   * @param bookname
   * @param price
   * @param ordernumber
   * @param totalprice
   */
  public Car(Integer userid, String bookname, BigDecimal price, Integer ordernumber, BigDecimal totalprice) {
    this.userid = userid;
    this.bookname = bookname;
    this.price = price;
    this.ordernumber = ordernumber;
    this.totalprice = totalprice;
  }
}
