package com.hxj.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
/**
 * @Date: 2021/1/18
 * @Author: 黄先军
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Books {

  private Integer id;
  private String bookname;
  private String author;
  private String press;
  private BigDecimal price;
  private Integer stock;
  private String note;
  private String image;
  private String isdeleted;

  /**
   * 添加图书的构造方法
   * @param bookname
   * @param author
   * @param press
   * @param price
   * @param stock
   * @param note
   * @param image
   */
  public Books(String bookname, String author, String press, BigDecimal price, Integer stock, String note, String image) {
    this.bookname = bookname;
    this.author = author;
    this.press = press;
    this.price = price;
    this.stock = stock;
    this.note = note;
    this.image = image;
  }

  /**
   * 修改图书的构造
   * @param id
   * @param bookname
   * @param author
   * @param press
   * @param price
   * @param stock
   * @param note
   * @param image
   */
  public Books(Integer id, String bookname, String author, String press, BigDecimal price, Integer stock, String note, String image) {
    this.id = id;
    this.bookname = bookname;
    this.author = author;
    this.press = press;
    this.price = price;
    this.stock = stock;
    this.note = note;
    this.image = image;
  }
}
