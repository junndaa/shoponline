package com.hxj.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;

/**
 * @Date: 2021/1/20
 * @Author: 黄先军
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Orderitems {
  private Integer id;
  private Integer orderid;
  private String bookname;
  private Integer ordernumber;
  private BigDecimal price;
  private BigDecimal totalprice;

  /**
   * 订单详情里面商品数据的构造方法
   * @param orderid
   * @param bookname
   * @param ordernumber
   * @param price
   * @param totalprice
   */
  public Orderitems(Integer orderid, String bookname, Integer ordernumber, BigDecimal price, BigDecimal totalprice) {
    this.orderid = orderid;
    this.bookname = bookname;
    this.ordernumber = ordernumber;
    this.price = price;
    this.totalprice = totalprice;
  }
}
