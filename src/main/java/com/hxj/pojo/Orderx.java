package com.hxj.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Date: 2021/1/20
 * @Author: 黄先军
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Orderx {
  private Integer id;
  private Integer userid;
  private BigDecimal allprice;
  private Date ordertime;
  private String address;
  private List<Orderitems> orderitemsList;

  /**
   * 添加订单的构造
   * @param userid
   * @param allprice
   * @param ordertime
   * @param address
   */
  public Orderx(Integer userid, BigDecimal allprice, Date ordertime, String address) {
    this.userid = userid;
    this.allprice = allprice;
    this.ordertime = ordertime;
    this.address = address;
  }
}
