package com.hxj.service;

import com.hxj.pojo.Books;

import java.util.List;
import java.util.Map;

/**
 * @Date: 2021/1/18
 * @Author: 黄先军
 * @Description:
 */
public interface BooksService {



    /**
     * 查询所有图书的信息
     * @return List<Books>
     */
    List<Books> selectAll();

    /**
     * 插入新的图书数据
     * @param book
     * @return
     */
    Integer insert(Books book);

    /**
     * 根据id删除图书的信息
     * @return
     */
    Integer delete(Integer id);

    /**
     * 通过id查询图书数据
     * @param id
     * @return
     */
    Books selectByid(Integer id);

    /**
     * 更新图书的数据
     * @param book
     * @return
     */
    Integer update(Books book);

    /**
     * 修改图片
     * @param id
     * @param imgPath
     * @return
     */
    Integer updatePhotoById(Integer id, String imgPath);

    /**
     * 遍历减库存
     * @param deleteStock
     * @return
     */
    Integer deleteStock(Map<String, Integer> deleteStock);
}
