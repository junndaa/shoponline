package com.hxj.service.impl;

import com.hxj.mapper.UserMapper;
import com.hxj.pojo.User;
import com.hxj.service.UserService;
import com.hxj.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * @Date: 2021/1/18
 * @Author: 黄先军
 * @Description:
 */
public class UserServiceImpl implements UserService {
    /**
     * 查询所有用户的信息
     * @return List<User>
     */
    @Override
    public List<User> selectAll() {
        List<User> users = null;
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        users = userMapper.selectAll();
        return users;
    }
    /**
     * 插入新是user数据,返回的是当前新插入数据的id值
     * @param user
     * @return
     */
    @Override
    public Integer insert(User user) {
        //加true可以插入，事务开启
        SqlSession sqlSession = MyBatisUtils.getSqlSession(true);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        userMapper.insert(user);
        //返回id
        Integer id = user.getId();
        return id;
    }

    /**
     * 根据用户id删除用户的信息
     * @return List<User>
     */
    @Override
    public Integer delete(Integer id) {
        //加了true就可以删除了，自动提交？，事务开启
        SqlSession sqlSession = MyBatisUtils.getSqlSession(true);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        Integer result = userMapper.delete(id);
        return result;
    }

    /**
     * 通过id查询user数据
     * @param id
     * @return
     */
    @Override
    public User selectByid(Integer id) {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        User user = userMapper.selectByid(id);
        return user;
    }

    /**
     * 更新user的数据
     * @param user
     * @return
     */
    @Override
    public Integer update(User user) {
        SqlSession sqlSession = MyBatisUtils.getSqlSession(true);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        Integer result = userMapper.update(user);
        return result;
    }

    /**
     * 登录验证身份
     * @param username
     * @param password
     * @param userrole
     * @return
     */
    @Override
    public Boolean loginVerify(String username, String password, String userrole) {
        Boolean loginFlag = false;
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        List<User> list = userMapper.selectAll();
        //遍历用户表进行登录验证
        for (User user : list) {
            if(user.getUsername().equals(username) && user.getPassword().equals(password) && user.getUserrole().equals(userrole)){
                loginFlag = true;
            }
        }
        return loginFlag;
    }
    /**
     * 通过用户名来查询用户的id，来添加商品到购物车
     * @param username
     * @return
     */
    @Override
    public Integer findIdByUsername(String username) {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        Integer idByUsername = userMapper.findIdByUsername(username);
        return idByUsername;
    }


}
