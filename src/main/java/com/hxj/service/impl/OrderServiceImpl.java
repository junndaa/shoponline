package com.hxj.service.impl;

import com.hxj.mapper.OrderMapper;
import com.hxj.pojo.Orderx;
import com.hxj.pojo.Orderitems;
import com.hxj.service.OrderService;
import com.hxj.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * @Date: 2021/1/20
 * @Author: 黄先军
 * @Description:
 */
public class OrderServiceImpl implements OrderService {
    /**
     * 插入新的order数据,返回的值是新插入的order的id
     * @param orderx
     * @return
     */
    @Override
    public Integer insert(Orderx orderx){
        SqlSession sqlSession = MyBatisUtils.getSqlSession(true);
        OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
        orderMapper.insert(orderx);
        Integer id = orderx.getId();
        return id;
    }

    /**
     * 批量插入orderitem数据
     * @param orderitemsList
     * @return
     */
    @Override
    public Integer insertList(List<Orderitems> orderitemsList){
        SqlSession sqlSession = MyBatisUtils.getSqlSession(true);
        OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
        Integer integer = orderMapper.insertList(orderitemsList);
        return integer;
    }
    /**
     * 通过id查询order数据，这个order数据里面包含了car的集合
     * @param orderId
     * @return
     */
    @Override
    public List<Orderx> selectByUserid(Integer orderId) {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
        List<Orderx> orderx = orderMapper.selectByUserid(orderId);
        return orderx;
    }

}
