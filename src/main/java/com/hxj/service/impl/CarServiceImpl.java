package com.hxj.service.impl;

import com.hxj.mapper.CarMapper;
import com.hxj.pojo.Car;
import com.hxj.service.CarService;
import com.hxj.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Date: 2021/1/19
 * @Author: 黄先军
 * @Description:
 */
public class CarServiceImpl implements CarService {
    /**
     * 查询购物车的信息
     * @return List<User>
     */
    @Override
    public List<Car> selectAll() {
        List<Car> cars = null;
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        cars = carMapper.selectAll();
        return cars;
    }
    /**
     * 往car表里面添加一行car数据，这个时候book里面的库存不需要减一
     * @param car
     * @return 返回的是当前插入car表里面的id
     */
    @Override
    public Integer addCar(Car car) {
        SqlSession sqlSession = MyBatisUtils.getSqlSession(true);
        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        Integer integer = carMapper.addCar(car);
        return integer;
    }
    /**
     * 查询需要加一的car表里面的数据
     * @param userid
     * @param bookname
     * @return 返回商品数量
     */
    @Override
    public Integer findOrdernumberByuseridAndBookname(Integer userid, String bookname){
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        Integer result = carMapper.findOrdernumberByuseridAndBookname(userid, bookname);
        return result;
    }
    /**
     * 更新car表里面的ordernumber数据
     * @param ordernumber
     * @param userid
     * @param bookname
     * @return
     */
    @Override
    public Integer updateCarOrdernumber(Integer ordernumber, Integer userid, String bookname, BigDecimal totalprice) {
        SqlSession sqlSession = MyBatisUtils.getSqlSession(true);
        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        Integer integer = carMapper.updateCarOrdernumber(ordernumber, userid, bookname,totalprice);
        return integer;
    }
    /**
     *通过用户id返回购物车数据
     * @param userId
     * @return
     */
    @Override
    public List<Car> findCarsByUserId(Integer userId) {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        List<Car> carsByUserId = carMapper.findCarsByUserId(userId);
        return carsByUserId;
    }
    /**
     *通过id删除car行数据操作
     * @param id
     * @return
     */
    @Override
    public Integer delete(Integer id){
        SqlSession sqlSession = MyBatisUtils.getSqlSession(true);
        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        Integer delete = carMapper.delete(id);
        return delete;
    }

    /**
     * 通过用户id清空购物车
     * @param anInt
     * @return
     */
    @Override
    public Integer cleanMyCar(int anInt) {
        SqlSession sqlSession = MyBatisUtils.getSqlSession(true);
        CarMapper carMapper = sqlSession.getMapper(CarMapper.class);
        Integer integer = carMapper.cleanMyCar(anInt);
        return integer;
    }
}
