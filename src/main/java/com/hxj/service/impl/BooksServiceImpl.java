package com.hxj.service.impl;

import com.hxj.mapper.BooksMapper;
import com.hxj.pojo.Books;
import com.hxj.service.BooksService;
import com.hxj.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;

import java.util.List;
import java.util.Map;

/**
 * @Date: 2021/1/18
 * @Author: 黄先军
 * @Description:
 */
public class BooksServiceImpl implements BooksService {
    /**
     * 查询所有图书的信息
     * @return List<Books>
     */
    @Override
    public List<Books> selectAll() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        BooksMapper booksMapper = sqlSession.getMapper(BooksMapper.class);
        List<Books> books = booksMapper.selectAll();
        return books;
    }

    /**
     * 插入新的图书数据
     * @param book
     * @return
     */
    @Override
    public Integer insert(Books book) {
        SqlSession sqlSession = MyBatisUtils.getSqlSession(true);
        BooksMapper booksMapper = sqlSession.getMapper(BooksMapper.class);
        Integer insert = booksMapper.insert(book);
        //这里返回的值是，新插入图书的自增的id
        return insert;
    }

    /**
     * 根据id删除图书的信息
     * @return
     */
    @Override
    public Integer delete(Integer id) {
        SqlSession sqlSession = MyBatisUtils.getSqlSession(true);
        BooksMapper booksMapper = sqlSession.getMapper(BooksMapper.class);
        Integer delete = booksMapper.delete(id);
        return delete;
    }

    /**
     * 通过id查询图书数据
     * @param id
     * @return
     */
    @Override
    public Books selectByid(Integer id) {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        BooksMapper booksMapper = sqlSession.getMapper(BooksMapper.class);
        Books books = booksMapper.selectByid(id);
        return books;
    }

    /**
     * 更新图书的数据
     * @param book
     * @return
     */
    @Override
    public Integer update(Books book) {
        SqlSession sqlSession = MyBatisUtils.getSqlSession(true);
        BooksMapper booksMapper = sqlSession.getMapper(BooksMapper.class);
        Integer update = booksMapper.update(book);
        return update;
    }

    /**
     * 修改图片
     * @param id
     * @param imgPath
     * @return
     */
    @Override
    public Integer updatePhotoById(Integer id, String imgPath) {
        SqlSession sqlSession = MyBatisUtils.getSqlSession(true);
        BooksMapper booksMapper = sqlSession.getMapper(BooksMapper.class);
        Integer integer = booksMapper.updatePhotoById(id, imgPath);
        return integer;
    }
    /**
     * 遍历减库存
     * @param map
     * @return
     */
    @Override
    public Integer deleteStock(Map<String, Integer> map){
        SqlSession sqlSession = MyBatisUtils.getSqlSession(true);
        BooksMapper booksMapper = sqlSession.getMapper(BooksMapper.class);
        for(Map.Entry<String, Integer> entry: map.entrySet()){
            booksMapper.reduceStock(entry.getKey(),entry.getValue());
        }
        return 1;
    }
}
