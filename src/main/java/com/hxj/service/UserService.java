package com.hxj.service;

import com.hxj.pojo.User;

import java.util.List;

/**
 * @Date: 2021/1/18
 * @Author: 黄先军
 * @Description:
 */
public interface UserService {
    /**
     * 查询所有
     * @return
     */
    List<User> selectAll();

    /**
     * 新增操作
     * @param user
     * @return
     */
    Integer insert(User user);

    /**
     *删除操作
     * @param id
     * @return
     */
    Integer delete(Integer id);

    /**
     *根据id查询
     * @param id
     * @return
     */
    User selectByid(Integer id);

    /**
     *修改操作
     * @param user
     * @return
     */
    Integer update(User user);

    /**
     * 登录验证身份  用户名+密码+角色 必须一致
     * @param username
     * @param password
     * @param userrole
     * @return
     */
    Boolean loginVerify(String username,String password,String userrole);

    /**
     * 通过用户名来查询用户的id，来添加商品到购物车
     * @param username
     * @return
     */
    Integer findIdByUsername(String username);
}
