package com.hxj.servlet;

import com.hxj.pojo.Books;
import com.hxj.pojo.User;
import com.hxj.service.BooksService;
import com.hxj.service.UserService;
import com.hxj.service.impl.BooksServiceImpl;
import com.hxj.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Date: 2021/1/18
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/bookFindByidServlet")
public class BookFindByidServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String bookId = req.getParameter("bookid");
        Integer anInt = null;
        //判断获取的bookId是否为空，为空的话就不转化
        if(bookId !=null && !bookId.equals("")){
            anInt = Integer.parseInt(bookId);
        }
        BooksService booksService = new BooksServiceImpl();
        //判断搜索的那里是否为空
        if(anInt!=null){
            Books book = booksService.selectByid(anInt);
            req.setAttribute("bookfindbyid",book);
            req.getRequestDispatcher("/bookfindbyid.jsp").forward(req,resp);
        }else{
            //查询所有用户的数据
            List<Books> book = booksService.selectAll();
            //把User信息封装到list中，list放在一个域中
            req.setAttribute("books",book);
            req.getRequestDispatcher("/adminindex.jsp").forward(req,resp);
        }
    }
}
