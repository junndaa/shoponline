package com.hxj.servlet;

import com.hxj.pojo.Books;
import com.hxj.service.BooksService;
import com.hxj.service.impl.BooksServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Date: 2021/1/18
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/bookDeleteServlet")
public class BookDeleteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //通过service来进行安全删除，将isdeleted的值改为1
        Integer id = Integer.parseInt(req.getParameter("id"));
        BooksService service = new BooksServiceImpl();
        Integer delete = service.delete(id);
        //删除失败给出提示
        if (!delete.equals(1)){
            resp.getWriter().write("删除失败，2秒后自动跳转到list界面");
        }
        //请求转发到管理员的主页面[管理商品的页面]
        BooksService booksService = new BooksServiceImpl();
        List<Books> list = booksService.selectAll();
        req.setAttribute("books",list);
        req.getRequestDispatcher("/adminindex.jsp").forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
