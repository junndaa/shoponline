package com.hxj.servlet;

import com.hxj.pojo.User;
import com.hxj.service.UserService;
import com.hxj.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Date: 2021/1/19
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/userAddServlet")
public class UserAddServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置编码格式，防止乱码
        req.setCharacterEncoding("UTF-8");
        //从前端获取参数
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String fullname = req.getParameter("fullname");
        String phonenumber = req.getParameter("phonenumber");
        String address = req.getParameter("address");
        String city = req.getParameter("city");
        String country = req.getParameter("country");
        String email = req.getParameter("email");
        String[] hobbys = req.getParameterValues("hobby");
        //遍历兴趣爱好，拼接字符串
        String hobby = "";
        for (String str : hobbys){
            if(str == ""){
                hobby = hobby + str;
            }else{
                hobby = hobby+",";
                hobby = hobby+ str;
            }
        }
        String secret = req.getParameter("secret");
        String secretAnswer = req.getParameter("secretanswer");
        //封装到user里面，然后调用service的添加用户的方法
        User user = new User(username,password,fullname,phonenumber,address,city,country,email,hobby,secret,secretAnswer);
        UserService userService = new UserServiceImpl();
        Integer insert = userService.insert(user);
        if(insert<=0){
            resp.getWriter().write("添加用户失败，2秒后自动跳转到登录界面界面");
        }
        //返回用户列表界面
        resp.sendRedirect(req.getContextPath()+"/userListServlet");
    }
}
