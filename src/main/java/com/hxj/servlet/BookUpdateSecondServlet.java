package com.hxj.servlet;

import com.hxj.pojo.Books;
import com.hxj.service.BooksService;
import com.hxj.service.impl.BooksServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * @Date: 2021/1/18
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/bookUpdateSecondServlet")
public class BookUpdateSecondServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置编码格式，防止前端传来的值乱码
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        //获取前端传来的值
        Integer id = Integer.parseInt(req.getParameter("id"));
        String bookName = req.getParameter("bookName");
        String author = req.getParameter("author");
        String press = req.getParameter("press");
        String price = req.getParameter("price");
        BigDecimal priceDecimal = new BigDecimal(price);
        Integer stock = Integer.parseInt(req.getParameter("stock"));
        String note = req.getParameter("note");
        String image = req.getParameter("image");
        //构造一本新的书出来
        Books book = new Books(id,bookName,author,press,priceDecimal,stock,note,image);
        BooksService booksService = new BooksServiceImpl();
        Integer update = booksService.update(book);
        //判断是否修改成功
        if (!update.equals(1)) {
            resp.getWriter().write("修改失败，2秒后自动跳转到用户界面");
        }
        //这里只能重定向
        resp.sendRedirect(req.getContextPath()+"/bookListServlet");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
