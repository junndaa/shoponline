package com.hxj.servlet;

import com.hxj.pojo.User;
import com.hxj.service.UserService;
import com.hxj.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Date: 2021/1/19
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/userListServlet")
public class UserListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserService userService = new UserServiceImpl();
        List<User> list = userService.selectAll();
        //设置返回的格式，避免乱码
        resp.setContentType("text/html;charset=UTF-8");
        //把User信息封装到list中，list放在一个域中
        req.setAttribute("users",list);
        req.getRequestDispatcher("/userlist.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
