package com.hxj.servlet;

import com.hxj.pojo.Orderx;
import com.hxj.service.OrderService;
import com.hxj.service.impl.OrderServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Date: 2021/1/21
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/orderListServlet")
public class OrderListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置编码格式，防止乱码
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        //通过session获取登录过后的用户id
        String loginUserId = req.getSession().getAttribute("loginUserId").toString();
        int userId = Integer.parseInt(loginUserId);
        OrderService orderService = new OrderServiceImpl();
        List<Orderx> orderList = orderService.selectByUserid(userId);
        //封装数据展示到orderList页面上
        req.setAttribute("ordeList",orderList);
        req.getRequestDispatcher("/orderList.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
