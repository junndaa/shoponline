package com.hxj.servlet;

import com.hxj.pojo.Car;
import com.hxj.service.CarService;
import com.hxj.service.impl.CarServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Date: 2021/1/19
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/carBackServlet")
public class CarBackServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id = Integer.parseInt(req.getParameter("id"));
        CarService carService = new CarServiceImpl();
        Integer delete = carService.delete(id);
        //通过session获取登录过后的用户id
        String loginUserId = req.getSession().getAttribute("loginUserId").toString();
        int anInt = Integer.parseInt(loginUserId);
        List<Car> carsByUserId = carService.findCarsByUserId(anInt);
        //把Car信息封装到carsByUserId中，carsByUserId放在一个域中
        req.setAttribute("carsByUserId",carsByUserId);
        req.getRequestDispatcher("/car.jsp").forward(req,resp);


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
