package com.hxj.servlet;

import com.hxj.pojo.User;
import com.hxj.service.UserService;
import com.hxj.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Date: 2021/1/19
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/userUpdateFirstServlet")
public class UserUpdateFirstServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取前端传来的userid
        String userid = req.getParameter("id");
        Integer anInt = null;
        //判断是否为空，然后再进行转化为int类型
        if(userid !=null && !userid.equals("")){
            anInt = Integer.parseInt(userid);
        }
        UserService userService = new UserServiceImpl();
        //判断搜索字段是否为空
        User user = userService.selectByid(anInt);
        //把数据放在域里面，然后转发到userlist页面上
        req.setAttribute("updateuser",user);
        req.getRequestDispatcher("/userupdate.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
