package com.hxj.servlet;

import com.hxj.pojo.User;
import com.hxj.service.UserService;
import com.hxj.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Date: 2021/1/19
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/userFindByIdServlet")
public class UserFindByIdServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取前端传来的userid
        String userid = req.getParameter("userid");
        Integer anInt = null;
        //判断是否为空，然后再进行转化为int类型
        if(userid !=null && !userid.equals("")){
            anInt = Integer.parseInt(userid);
        }
        UserService userService = new UserServiceImpl();
        //判断搜索字段是否为空
        if(anInt!=null){
            User user = userService.selectByid(anInt);
            //因为el的foreach表达式不能遍历单个数据，所以这里需要把user放到list里面才可以
            List<User> list = new ArrayList<>();
            list.add(user);
            //把数据放在域里面，然后转发到userlist页面上
            req.setAttribute("users",list);
            req.getRequestDispatcher("/userlist.jsp").forward(req,resp);
        }else{
            //重新定向到userlist.jsp里面去
            resp.sendRedirect("/userListServlet");
        }
    }
}
