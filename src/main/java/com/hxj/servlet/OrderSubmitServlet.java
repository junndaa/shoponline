package com.hxj.servlet;

import com.hxj.pojo.Car;
import com.hxj.pojo.Orderx;
import com.hxj.pojo.Orderitems;
import com.hxj.service.BooksService;
import com.hxj.service.CarService;
import com.hxj.service.OrderService;
import com.hxj.service.impl.BooksServiceImpl;
import com.hxj.service.impl.CarServiceImpl;
import com.hxj.service.impl.OrderServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Date: 2021/1/20
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/orderSubmitServlet")
public class OrderSubmitServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置编码格式，防止乱码
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        //通过session获取登录过后的用户id
        String loginUserId = req.getSession().getAttribute("loginUserId").toString();
        int userId = Integer.parseInt(loginUserId);
        //获取价格转化为decimal
        String price = req.getParameter("allPrice");
        BigDecimal allPrice = new BigDecimal(price);
        //获取下单时间和收获地址
        String time = req.getParameter("nowTime");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date orderTime = null;
        try {
            orderTime = df.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String address = req.getParameter("address");
        //因为get方法默认是"ISO-8859-1"编码，强制转换为utf-8
        address = new String(address.getBytes("ISO-8859-1"),"utf-8");
        //构建一个order
        Orderx orderx = new Orderx(userId,allPrice,orderTime,address);
        //先存order，然后同order返回orderid来继续往orderitems里面插入car数据
        OrderService orderService = new OrderServiceImpl();
        //执行添加操作
        Integer orderId = orderService.insert(orderx);
        if(orderId > 0){
            //获取用户的购物车数据
            CarService carService = new CarServiceImpl();
            List<Car> cars = carService.findCarsByUserId(userId);
            //构建一个ArryList用来执行批量插入
            List<Orderitems> orderitemsList = new ArrayList<Orderitems>();
            //构建一个Map来执行减库存操作
            Map<String,Integer> deleteStock = new HashMap<String,Integer>();
            for (Car car : cars) {
                String bookname = car.getBookname();
                Integer ordernumber = car.getOrdernumber();
                BigDecimal price1 = car.getPrice();
                BigDecimal totalprice = car.getTotalprice();
                Orderitems orderitems =new Orderitems(orderId,bookname,ordernumber,price1,totalprice);
                orderitemsList.add(orderitems);
                deleteStock.put(bookname,ordernumber);
            }
            //执行批量插入操作
            orderService.insertList(orderitemsList);
            //执行库存数量减的操作
            BooksService booksService = new BooksServiceImpl();
            booksService.deleteStock(deleteStock);
            //这里加一个清空购物车
            //重定向到我的订单页面
            resp.sendRedirect("/orderListServlet");
        }else{
            resp.setContentType("text/html;charset=UTF-8");
            resp.getWriter().write("下单失败，4秒后自动跳转到商品界面");
            resp.sendRedirect("/carListServlet");
        }


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
