package com.hxj.servlet;

import com.hxj.pojo.Books;
import com.hxj.service.BooksService;
import com.hxj.service.impl.BooksServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Date: 2021/1/20
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/shopIndexServlet")
public class ShopIndexServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //从数据库获取图书信息，方便下面两个判断
        BooksService booksService = new BooksServiceImpl();
        List<Books> list = booksService.selectAll();
        req.setAttribute("books",list);
        req.getRequestDispatcher("/userindex.jsp").forward(req,resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
