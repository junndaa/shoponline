package com.hxj.servlet;

import com.hxj.pojo.Books;
import com.hxj.service.BooksService;
import com.hxj.service.impl.BooksServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Date: 2021/1/19
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/bookListServlet")
public class BookListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //构建service来查询数据库里面的图书信息
        BooksService booksService = new BooksServiceImpl();
        List<Books> list = booksService.selectAll();
        //请求转发，把商品数据显示到前端页面上去
        req.setAttribute("books",list);
        req.getRequestDispatcher("/adminindex.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
