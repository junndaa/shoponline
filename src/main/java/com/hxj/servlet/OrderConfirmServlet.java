package com.hxj.servlet;

import com.hxj.pojo.Car;
import com.hxj.service.CarService;
import com.hxj.service.impl.CarServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Date: 2021/1/20
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/orderConfirmServlet")
public class OrderConfirmServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //通过session获取登录过后的用户id
        String loginUserId = req.getSession().getAttribute("loginUserId").toString();
        int anInt = Integer.parseInt(loginUserId);
        //通过session里面的id来查询该用户的购物车，然后点击提交到确认订单页面
        CarService carService = new CarServiceImpl();
        List<Car> cars = carService.findCarsByUserId(anInt);
        //获取当前时间
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String format = df.format(new Date());
        //把购物车数据传到确认订单页面
        req.setAttribute("nowTime",format);
        req.setAttribute("carsList",cars);
        req.getRequestDispatcher("/confirmOrder.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
