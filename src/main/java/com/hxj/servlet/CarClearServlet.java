package com.hxj.servlet;

import com.hxj.pojo.Car;
import com.hxj.service.CarService;
import com.hxj.service.impl.CarServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Date: 2021/1/20
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/carClearServlet")
public class CarClearServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //通过session获取登录过后的用户id
        String loginUserId = req.getSession().getAttribute("loginUserId").toString();
        int anInt = Integer.parseInt(loginUserId);
        //通过id清空购物车
        CarService carService = new CarServiceImpl();
        carService.cleanMyCar(anInt);
        //清空后,刷新该页面
        List<Car> carsByUserId = carService.findCarsByUserId(anInt);
        //把Car信息封装到carsByUserId中，carsByUserId放在一个域中
        req.setAttribute("carsByUserId",carsByUserId);
        req.getRequestDispatcher("/car.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
