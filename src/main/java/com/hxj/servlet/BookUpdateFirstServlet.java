package com.hxj.servlet;

import com.hxj.pojo.Books;
import com.hxj.pojo.User;
import com.hxj.service.BooksService;
import com.hxj.service.UserService;
import com.hxj.service.impl.BooksServiceImpl;
import com.hxj.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Date: 2021/1/18
 * @Author: 黄先军
 * @Description: 通过id查询图书信息显示到页面上
 */
@WebServlet("/bookUpdateFirstServlet")
public class BookUpdateFirstServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取前端传来的bookId
        String bookId = req.getParameter("id");
        Integer anInt = null;
        //判断是否为空，然后再进行转化为int类型
        if(bookId !=null && !bookId.equals("")){
            anInt = Integer.parseInt(bookId);
        }
        BooksService booksService = new BooksServiceImpl();
        Books books = booksService.selectByid(anInt);
        //把数据放在域里面，然后转发到bookupdate页面上
        req.setAttribute("updateBook",books);
        req.getRequestDispatcher("/bookupdate.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
