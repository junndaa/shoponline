package com.hxj.servlet;

import com.hxj.pojo.Books;
import com.hxj.service.BooksService;
import com.hxj.service.impl.BooksServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * @Date: 2021/1/18
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/bookAddServlet")
public class BookAddServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置格式
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        //获取前端传来的数据信息
        String bookname = req.getParameter("bookname");
        String author = req.getParameter("author");
        String press = req.getParameter("press");
        //添加图书会有乱码，在这里转化一下
        bookname = new String(bookname.getBytes("ISO-8859-1"),"utf-8");
        author = new String(author.getBytes("ISO-8859-1"),"utf-8");
        press = new String(press.getBytes("ISO-8859-1"),"utf-8");
        Integer price = Integer.parseInt(req.getParameter("price"));
        BigDecimal priceDecimal = new BigDecimal(price);
        Integer stock = Integer.parseInt(req.getParameter("stock"));
        String note = req.getParameter("note");
        String image = req.getParameter("image");
        //构建图书信息，执行添加操作
        Books book = new Books(bookname,author,press,priceDecimal,stock,note,image);
        BooksService booksService = new BooksServiceImpl();
        Integer insert = booksService.insert(book);
        //判断添加成功与否
        if(insert<=0){
            resp.setContentType("text/html;charset=UTF-8");
            resp.getWriter().write("添加图书失败，2秒后自动跳转到图书添加界面");
            resp.setHeader("Refresh","3;URL=bookadd.jsp");
        }else{
            //添加成功跳转到book展示页面
            resp.sendRedirect(req.getContextPath()+"/bookListServlet");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
