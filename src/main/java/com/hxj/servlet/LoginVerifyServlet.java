package com.hxj.servlet;

import com.hxj.entity.StatusCode;
import com.hxj.pojo.Books;
import com.hxj.service.BooksService;
import com.hxj.service.UserService;
import com.hxj.service.impl.BooksServiceImpl;
import com.hxj.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @Date: 2021/1/18
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/loginVerifyServlet")
public class LoginVerifyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //这里必须是dopost，不然通过login.jsp提交的post请求发送不过来；
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取前端页面传来的值进行判断
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String userrole = req.getParameter("userrole");
        //判断是否登录成功，成功loginFlag=true，失败loginFlag=false
        UserService userService = new UserServiceImpl();
        Boolean loginFlag = userService.loginVerify(username, password, userrole);
        if(loginFlag){
            //从数据库获取图书信息，方便下面两个判断
            BooksService booksService = new BooksServiceImpl();
            List<Books> list = booksService.selectAll();
            //当勾选了管理员，进入管理员界面
            if(userrole.equals(StatusCode.ADMIN_NUMBER)){
                //请求转发，把商品数据传到管理员页面上去
                req.setAttribute("books",list);
                req.getRequestDispatcher("/adminindex.jsp").forward(req,resp);
            }
            //当勾选了普通用户，进入普通用户页面
            else if(userrole.equals( StatusCode.USER_NUMBER)){
                Integer loginId = userService.findIdByUsername(username);
                //请求转发，把商品数据传到用户主页页面上去
                req.setAttribute("loginId",loginId);
                //存一个session，用来存登录用户的id，用户登录后的一些操作
                HttpSession session = req.getSession();
                session.setAttribute("loginUserId",loginId);
                //获取图书信息展示到前端页面上
                req.setAttribute("books",list);
                req.getRequestDispatcher("/userindex.jsp").forward(req,resp);
            }
        }else{
            resp.setContentType("text/html;charset=UTF-8");
            resp.getWriter().write("登录失败，2秒后自动跳转到登陆界面");
            resp.setHeader("Refresh","3;URL=login.jsp");
        }
    }

}
