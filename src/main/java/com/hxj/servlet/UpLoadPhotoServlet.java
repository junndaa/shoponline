package com.hxj.servlet;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * @Date: 2021/1/20
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/upLoadPhotoServlet")
public class UpLoadPhotoServlet extends HttpServlet {
        private static final long serialVersionUID = 1L;

        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
            doPost(request, response);
        }

        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
            String fileName=null;
            //图片上传工厂
            DiskFileItemFactory factory=new DiskFileItemFactory();
            //文件上传处理器
            ServletFileUpload upload=new ServletFileUpload(factory);
            upload.setHeaderEncoding("UTF-8");
            upload.setSizeMax(1024*1024);
            //定义一个flag用来检查上传图片成功与否
            boolean flag = false;
            String imgPath =null;
            try {
                //解码请求 得到所有表单元素
                List<FileItem> items= upload.parseRequest(request);
                for (FileItem item : items) {
                    // 有可能是 文件，也可能是普通文字这个选项是 文字
                    if (item.isFormField()) {
                        System.out.println("表单值为："+item.getString());
                        // 是文件
                    }else{
                        //获取图片后缀名
                        String format=item.getName().substring(item.getName().indexOf("."));
                        //图片命名
                        fileName= UUID.randomUUID().toString().replaceAll("-","")+format;
                        String basePath =  request.getSession().getServletContext().getRealPath("/images");
                        //把文件的路径保存到session里面
                         imgPath = "images/"+fileName;
                        //打印一下文件路径
                        System.out.println("文件路径是："+basePath);
                        //文件上传
                        item.write(new File(basePath,fileName));
                        flag = true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(flag){
                //请求转发到添加页面上，并提示图片添加成功，把图片路径显示到图片上
                request.setAttribute("imgPath",imgPath);
                request.getRequestDispatcher("/bookadd.jsp").forward(request,response);
            }else{
                response.setContentType("text/html;charset=UTF-8");
                response.getWriter().write("图片上传失败，2秒后自动跳转到登陆界面");
                response.setHeader("Refresh","3;URL=bookadd.jsp");
            }
        }





}
