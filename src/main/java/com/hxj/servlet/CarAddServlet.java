package com.hxj.servlet;

import com.hxj.pojo.Books;
import com.hxj.pojo.Car;
import com.hxj.service.BooksService;
import com.hxj.service.CarService;
import com.hxj.service.UserService;
import com.hxj.service.impl.BooksServiceImpl;
import com.hxj.service.impl.CarServiceImpl;
import com.hxj.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Date: 2021/1/19
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/carAddServlet")
public class CarAddServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置编码格式，防止前端传来的值乱码
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        //构造相应的service
        BooksService booksService = new BooksServiceImpl();
        CarService carService = new CarServiceImpl();
        //获取前端传来的，用来查询
        Integer bookId = Integer.parseInt(req.getParameter("bookId"));
        Integer loginId = Integer.parseInt(req.getParameter("loginId"));
        String bookname = req.getParameter("name");
        //开始查询信息并组合
        Books book = booksService.selectByid(bookId);
        //查询car里面的数据，来遍历查询car里面是否有图书和用户对应的数据
        List<Car> cars = carService.selectAll();
        //定义一个遍历查询car表里面是否含有userid和bookname对应的值，有就执行car的购买数量加一，没有就执行car添加
        boolean carFlag = false;
        for (Car car : cars) {
            if(car.getUserid().equals(loginId) && car.getBookname().equals(bookname)){
                carFlag = true;
            }
        }
        //为true就执行添加操作，为false就执行跟新操作
        Integer result = null;
        if(carFlag){
            //根据userid和bookname查询car表里面的购买数量
            Integer ordernumber = carService.findOrdernumberByuseridAndBookname(loginId, bookname);
            ordernumber = ordernumber + 1;
            //更新car表里面的金额totality
            BigDecimal price = book.getPrice();
            BigDecimal number = new BigDecimal(ordernumber);
            BigDecimal totality = price.multiply(number);
            //更新car表里面的数据
            result = carService.updateCarOrdernumber(ordernumber, loginId, bookname,totality);
        }else {
            Car car = new Car(loginId,book.getBookname(),book.getPrice(),1,book.getPrice());
            result = carService.addCar(car);
        }
        //要留在当前页面
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out=resp.getWriter();
        //获取商品数据
        List<Books> list = booksService.selectAll();
        if(result>0){
            //提示框添加商品成功
            out.print("<script>alert('添加成功!')</script>");
        }else{
            //提示框添加商品失败
            out.print("<script>alert('添加失败!')</script>");
        }
        req.setAttribute("books",list);
        req.getRequestDispatcher("/userindex.jsp").forward(req,resp);


    }
}
