package com.hxj.servlet;

import com.hxj.pojo.User;
import com.hxj.service.UserService;
import com.hxj.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Date: 2021/1/19
 * @Author: 黄先军
 * @Description:
 */
@WebServlet("/userUpdateSecondServlet")
public class UserUpdateSecondServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置编码格式，防止前端传来的值乱码
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        //获取前端传来的数据
        Integer id = Integer.parseInt(req.getParameter("id"));
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String fullName = req.getParameter("fullname");
        String email = req.getParameter("email");
        String hobby = req.getParameter("hobby");
        String secret = req.getParameter("secret");
        String secretAnswer = req.getParameter("secretanswer");
        String phoneNumber = req.getParameter("phonenumber");
        User user = new User(id,username,password,fullName,phoneNumber,email,hobby,secret,secretAnswer);
        UserService userService = new UserServiceImpl();
        //执行修改操作
        Integer update = userService.update(user);
        //判断是否修改成功
        if (!update.equals(1)) {
            resp.getWriter().write("修改失败，2秒后自动跳转到用户界面");
        }
        //这里只能重定向
        resp.sendRedirect(req.getContextPath()+"/userListServlet");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
