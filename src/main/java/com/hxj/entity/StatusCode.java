package com.hxj.entity;

/**
 * @Date: 2021/1/18
 * @Author: 黄先军
 * @Description:
 */
public class StatusCode {
    /**
     * 管理员登录和用户登录时的选值
     */
    public static final String ADMIN_NUMBER = "1";
    public static final String USER_NUMBER = "0";
}
