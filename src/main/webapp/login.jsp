<%--
  Created by IntelliJ IDEA.
  User: 军大
  Date: 2021/1/18
  Time: 14:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录</title>
</head>
<body>
    <form action="/loginVerifyServlet" method="post"  autocomplete="off">
        <div>
            <table border="1px solid" align="center">
                <tr>
                    <td colspan="2" align="center"><h4>用户登录</h4></td>
                </tr>
                <tr>
                    <td>用户名:</td>
                    <td><input type="text" name="username" /></td>
                </tr>
                <tr>
                    <td>密&nbsp;&nbsp;&nbsp;&nbsp;码:</td>
                    <td><input type="password" name="password" /></td>
                </tr>
                <tr>
                    <td><input type="radio" name="userrole" value="1" />管理员</td>
                    <td><input type="radio" name="userrole" value="0" checked="checked" />普通用户</td>
                </tr>
                <tr>
                    <td align="right" ><input type="submit" value="登录" /></td>
                    <td><input type="button" onclick="addUser()" value="注册"/></td>
                </tr>
            </table>
        </div>
    </form>
</body>

</html>
<script>
    function addUser() {
        window.location.href="userregister.jsp";
    }
</script>
