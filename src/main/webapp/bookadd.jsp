<%--
  Created by IntelliJ IDEA.
  User: 军大
  Date: 2021/1/18
  Time: 18:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false"%>
<html>
<head>
    <title>添加图书</title>
</head>
<body>
<form method="post" action="/upLoadPhotoServlet"  enctype="multipart/form-data">
    <div align="center">
        <span>
              <label> 单张照片</label>
        </span>
        <span>
              <input name="file"  type="file"  required="required" value="选择图片" >
        </span>
        <input type="submit" value="点击上传图片" name="tijiao">
    </div>
</form>
<form action="/bookAddServlet" method="get" autocomplete="off" >
    <table align="center">
        <tr>
            <td>图书名:<input type="text" name="bookname"></td>
        </tr>
        <tr>
            <td>作者:<input type="text" name="author"></td>
        </tr>
        <tr>
            <td>出版社:<input type="text" name="press"></td>
        </tr>
        <tr>
            <td>价格:<input type="text" name="price"></td>
        </tr>
        <tr>
            <td>库存:<input type="text" name="stock"></td>
        </tr>
        <tr>
            <td>描述:<input type="text" name="note"></td>
        </tr>
            <td>
                图片:<img src="${requestScope.imgPath}" width="71.5" height="100"/>
                    <input type="hidden" value="${requestScope.imgPath}" name="image">
            </td>
        </tr>
        <tr>
            <td><input type="submit" value="确认添加图书"></td>
        </tr>
    </table>
</form>

</body>
</html>
