<%--
  Created by IntelliJ IDEA.
  User: 军大
  Date: 2021/1/18
  Time: 16:51
  To change this template use File | Settings | File Templates.
--%>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<html>
<head>
    <title>管理员主页</title>
</head>
<body>
<table border="1px solid" align="center">
    <tr>
        <form  action="/bookFindByidServlet" method="post" autocomplete="off">
            <td colspan="2">
                图书id:<input type="text" name="bookid"/>
                <input type="submit" value="搜索">
            </td>
        </form>
        <td>
            <a href="login.jsp">用户退出</a>
        </td>

        <td><a href="/userListServlet">用户信息</a></td>
        <td><a href="bookadd.jsp">添加图书</a></td>
    </tr>
    <tr>
        <td bgcolor="#6495ed">商品id</td>
        <td bgcolor="#6495ed">图片</td>
        <td bgcolor="#6495ed">商品摘要</td>
        <td bgcolor="#6495ed">信息修改</td>
        <td bgcolor="#6495ed">删除操作</td>
    </tr>
    <c:forEach items="${requestScope.books}" var="books">
        <tr>
            <td>${books.id}</td>
            <%--这里有个bug--%>
            <td><img src="${books.image}" width="71.5" height="100"/></td>
            <td>
                  <span>书名:${books.bookname}<span/><br/>
                  <span>作者:${books.author}<span/><br/>
                  <span>出版社:${books.press}<span/><br/>
                  <span>价格:${books.price}元<span/>
            </td>
            <td>
                <a href="/bookUpdateFirstServlet?id=${books.id}">修改图书信息</a>
            </td>
            <td>
                <a href="/bookDeleteServlet?id=${books.id}">删除</a>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
