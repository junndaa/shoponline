<%--
  Created by IntelliJ IDEA.
  User: 军大
  Date: 2021/1/21
  Time: 11:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<html>
<head>
    <title>我的订单</title>
</head>
<body>
<c:forEach items="${requestScope.ordeList}" var="orderlist">
<table border="1px solid" align="center">
    <tr>
        <td bgcolor="#6495ed">商品名称</td>
        <td bgcolor="#6495ed">商品单价</td>
        <td bgcolor="#6495ed">购买数量</td>
        <td bgcolor="#6495ed">金额</td>
    </tr>
    <c:forEach items="${orderlist.orderitemsList}" var="orderitem">
        <tr>
            <td>${orderitem.bookname}</td>
            <td>${orderitem.price}</td>
            <td>${orderitem.ordernumber}</td>
            <td>${orderitem.totalprice}</td>
        </tr>
    </c:forEach>
    <tr>
        <td colspan="2">
            总计:${orderlist.allprice}
        </td>
        <td colspan="2">
            下单时间:${orderlist.ordertime}
        </td>
    </tr>
    <tr>
        <td colspan="4">
            收获地址:${orderlist.address}
        </td>
    </tr>
</table>
</c:forEach>
<table align="center" border="1px solid" >
    <tr>
        <td><a href="/shopIndexServlet">返回首页</a></td>
    </tr>
</table>
</body>
</html>
