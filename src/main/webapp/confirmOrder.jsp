
<%--
  Created by IntelliJ IDEA.
  User: 军大
  Date: 2021/1/20
  Time: 17:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false"%>
<html>
<head>
    <title>订单确认</title>
</head>
<body>
<form action="/orderSubmitServlet" method="get">
    <table border="1px solid" align="center">
        <tr>
            <td bgcolor="#6495ed">商品名称</td>
            <td bgcolor="#6495ed">商品单价</td>
            <td bgcolor="#6495ed">购买数量</td>
            <td bgcolor="#6495ed">金额</td>
        </tr>
        <c:forEach items="${requestScope.carsList}" var="cars">
            <tr>
                <td>${cars.bookname}</td>
                <td>${cars.price}</td>
                <td>${cars.ordernumber}</td>
                <td>${cars.totalprice}</td>
                <c:set var="total"  value="${total+cars.totalprice}"></c:set>
            </tr>
        </c:forEach>
        <tr>
            <td colspan="2" align="center">总计:<input type="text" readonly="readonly" value="${total}" name="allPrice"></td>
            <td colspan="2"> 下单时间<input type="text" name="nowTime" readonly="readonly" value="${requestScope.nowTime}"></td>
        </tr>
        <tr>
            <td align="center">收货地址:<input type="text" name="address"></td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <a href="/shopIndexServlet">继续购物</a>
                <a href="#" onClick="javascript :history.go(-1);">返回购物车</a>
                <input type="submit" value="确认下单" />
            </td>
        </tr>
    </table>
</form>
</body>
</html>
