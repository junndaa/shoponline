<%--
  Created by IntelliJ IDEA.
  User: 军大
  Date: 2021/1/18
  Time: 16:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<html>
<head>
    <title>普通用户主页</title>
</head>
<body>
    <table border="1px solid" align="center">
        <tr>
            <td colspan="3">
                <label>欢迎您:<%=request.getSession().getAttribute("loginUserId")%></label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <a href="javascript:location.reload();">首页</a>
                <a href="/carListServlet">购物车</a>
                <a href="/orderListServlet">我的订单</a>
                <a href="login.jsp">用户退出</a>
            </td>
        </tr>
        <tr>
            <td bgcolor="#6495ed">商品图片</td>
            <td bgcolor="#6495ed">商品摘要</td>
            <td bgcolor="#6495ed">在线购买</td>
        </tr>
        <c:forEach items="${requestScope.books}" var="books">
            <tr>
                <%--这里有个bug--%>
                <td><img src="${books.image}" width="71.5" height="100"/></td>
                <td>
                  <span>书名:${books.bookname}<span/><br/>
                  <span>作者:${books.author}<span/><br/>
                  <span>出版社:${books.press}<span/><br/>
                  <span>价格:${books.price}元<span/>
                </td>
                <td>
                    <form action="/carAddServlet" method="post" autocomplete="off">
                        <input type="hidden" name="bookId" value="${books.id}">
                        <input type="hidden" name="name" value="${books.bookname}">
                        <input type="hidden" name="loginId" value="<%=request.getSession().getAttribute("loginUserId")%>">
                        <input type="submit" value="放入购物车">
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
