<%--
  Created by IntelliJ IDEA.
  User: 军大
  Date: 2021/1/18
  Time: 20:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false"%>
<html>
<head>
    <title>单个图书信息</title>
</head>
<body>
    <table border="1px solid" align="center">
        <tr>
            <td bgcolor="#6495ed">图书id</td>
            <td bgcolor="#6495ed">图书名</td>
            <td bgcolor="#6495ed">图书作者</td>
            <td bgcolor="#6495ed">出版社</td>
            <td bgcolor="#6495ed">图书价格</td>
            <td bgcolor="#6495ed">图书数量</td>
            <td bgcolor="#6495ed">图像信息</td>
            <td bgcolor="#6495ed">操作</td>
        </tr>
        <tr>
            <td>${requestScope.bookfindbyid.id}</td>
            <td>${requestScope.bookfindbyid.bookname}</td>
            <td>${requestScope.bookfindbyid.author}</td>
            <td>${requestScope.bookfindbyid.press}</td>
            <td>${requestScope.bookfindbyid.price}</td>
            <td>${requestScope.bookfindbyid.stock}</td>
            <td>${requestScope.bookfindbyid.image}</td>
            <td><a href="/bookDeleteServlet?id=${requestScope.bookfindbyid.id}">删除</a></td>
        </tr>
        <tr>
            <td colspan="4"><a href="bookadd.jsp">添加图书信息</a></td>
            <td colspan="4"><a href="#" onClick="javascript :history.go(-1);">返回上一层</a></td>
        </tr>
    </table>
</body>
</html>
