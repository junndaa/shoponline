<%--
  Created by IntelliJ IDEA.
  User: 军大
  Date: 2021/1/19
  Time: 11:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<html>
<head>
    <title>购物车</title>
</head>
<body>
<table border="1px solid" align="center">
    <tr>
        <td bgcolor="#6495ed">商品名称</td>
        <td bgcolor="#6495ed">商品单价</td>
        <td bgcolor="#6495ed">购买数量</td>
        <td bgcolor="#6495ed">金额</td>
        <td bgcolor="#6495ed">编辑</td>
    </tr>
    <c:forEach items="${requestScope.carsByUserId}" var="cars">
        <tr>
            <td>${cars.bookname}</td>
            <td>${cars.price}</td>
            <td>${cars.ordernumber}</td>
            <td>${cars.totalprice}</td>
            <td>
                <a href="/carBackServlet?id=${cars.id}">退回商品架子</a>
            </td>
        </tr>
    </c:forEach>
    <tr>
        <td colspan="5">
            <a href="/shopIndexServlet">继续购物</a>
            <a href="/carClearServlet">清空购物车</a>
            <a href="/orderConfirmServlet">提交订单</a>
        </td>
    </tr>
</table>
</body>
</html>
