<%--
  Created by IntelliJ IDEA.
  User: 军大
  Date: 2021/1/19
  Time: 14:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<html>
<head>
    <title>用户管理</title>
</head>
<body>
<table border="1px solid">
    <tr>
        <form  action="/userFindByIdServlet" method="post" autocomplete="off">
            <td colspan="4">
                用户id:<input type="text" name="userid" />
                <input type="submit" value="查询">
            </td>
        </form>
        <td colspan="3"><a href="/bookListServlet">图书信息</a></td>
        <td colspan="4"><a href="useradd.jsp">添加用户</a></td>
    </tr>
    <tr>
        <td>id</td>
        <td>用户名</td>
        <td>用户密码</td>
        <td>用户姓名</td>
        <td>用户电话</td>
        <td>用户邮箱</td>
        <td>用户爱好</td>
        <td>密保问题</td>
        <td>密保答案</td>
        <td>修改操作</td>
        <td>删除操作</td>
    </tr>
    <c:forEach items="${requestScope.users}" var="user">
        <tr>
            <td>${user.id}</td>
            <td>${user.username}</td>
            <td>${user.password}</td>
            <td>${user.fullname}</td>
            <td>${user.phonenumber}</td>
            <td>${user.email}</td>
            <td>${user.hobby}</td>
            <td>${user.secret}</td>
            <td>${user.secretanswer}</td>
            <td>
                <a href="/userUpdateFirstServlet?id=${user.id}">更新用户</a>
            </td>
            <td>
                <a href="/userDeleteServlet?id=${user.id}">删除用户</a>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
