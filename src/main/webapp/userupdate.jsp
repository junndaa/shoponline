<%--
  Created by IntelliJ IDEA.
  User: 军大
  Date: 2021/1/19
  Time: 15:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false"%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form action="/userUpdateSecondServlet" method="post" autocomplete="off">
    <table align="center">
        <tr>
            <td>用户id</td>
            <td>用户名</td>
            <td>用户密码</td>
            <td>姓名</td>
            <td>用户邮箱</td>
            <td>用户爱好</td>
        </tr>
        <tr>
            <td><input type="text" name="id" readonly="readonly" value="${requestScope.updateuser.id}"></td>
            <td><input type="text" name="username" value="${requestScope.updateuser.username}"></td>
            <td><input type="password" name="password" value="${requestScope.updateuser.password}"></td>
            <td><input type="text" name="fullname" value="${requestScope.updateuser.fullname}"></td>
            <td><input type="text" name="email" value="${requestScope.updateuser.email}"></td>
            <td><input type="text" name="hobby" value="${requestScope.updateuser.hobby}"></td>
        </tr>
        <tr>
            <td>密保问题</td>
            <td>密保答案</td>
            <td>联系电话</td>
            <td>确认修改</td>
            <td>返回</td>
        </tr>
        <tr>
            <td><input type="text" name="secret" value="${requestScope.updateuser.secret}"></td>
            <td><input type="text" name="secretanswer" value="${requestScope.updateuser.secretanswer}"></td>
            <td><input type="text" name="phonenumber" value="${requestScope.updateuser.phonenumber}"></td>
            <td><input type="submit" value="确认修改"></td>
            <td><a href="#" onClick="javascript :history.go(-1);">返回上一层</a></td>
        </tr>
    </table>
</form>
</body>
</html>
