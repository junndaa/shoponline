<%--
  Created by IntelliJ IDEA.
  User: 军大
  Date: 2021/1/18
  Time: 20:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false"%>

<html>
<head>
    <title>修改图书</title>
</head>
<body>
<form method="post" action="/bookUpdateImgServlet?updateimgid=${requestScope.updateBook.id}"  enctype="multipart/form-data">
    <div align="center">
        <span>
              <label> 单张照片</label>
        </span>
        <span>
              <input name="file"  type="file"  required="required" value="选择图片" >
        </span>
        <input type="submit" value="点击修改图片" name="tijiao">
    </div>
</form>
<form action="/bookUpdateSecondServlet?id=${requestScope.updateBook.id}" method="post" autocomplete="off" >
    <table align="center">
        <tr>
            <td>图书名:<input type="text" name="bookName" value="${requestScope.updateBook.bookname}"></td>
        </tr>
        <tr>
            <td>作者:<input type="text" name="author" value="${requestScope.updateBook.author}"></td>
        </tr>
        <tr>
            <td>出版社:<input type="text" name="press" value="${requestScope.updateBook.press}"></td>
        </tr>
        <tr>
            <td>价格:<input type="text" name="price" value="${requestScope.updateBook.price}"></td>
        </tr>
        <tr>
            <td>库存:<input type="text" name="stock" value="${requestScope.updateBook.stock}"></td>
        </tr>
        <tr>
            <td>描述:<input type="text" name="note" value="${requestScope.updateBook.note}"></td>
        </tr>
        <td>
            图片:<img src="${requestScope.updateBook.image}" width="71.5" height="100"/>
            <input type="hidden" value="${requestScope.updateBook.image}" name="image">
        </td>
        </tr>
        <tr>
            <td><input type="submit" value="确认修改图书"></td>
        </tr>
    </table>
</form>
</body>
</html>
