<%--
  Created by IntelliJ IDEA.
  User: 军大
  Date: 2021/1/18
  Time: 17:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>用户注册</title>
    <style>
        body{
            background-color: #8d8a8a;
            align:center;
        }
        table{
            width:800px;
            height:auto;

        }
        #pass{
            width:200px;
            height:18px;
        }
        .td-left{
            width:100px;
        }
        .content{
            width:200px;
        }
        .warning{
            width:200px;
            color:red;
            size:2px;
        }
        .right-color{
            color:green;
        }
        .text{
            width:200px;
            height:18px;
        }
        /* 按钮美化 */
        .button {
            width: 270px;
            height: 40px;
            border-width: 0px;
            border-radius: 15px;
            background:#FF6699;
            cursor: pointer;
            outline: none;
            font-family: 微软雅黑;
            color: white;
            font-size: 17px;
        }
        /* 鼠标移入按钮范围时改变颜色 */
        .button:hover {
            background: #4dd52e;
        }

        .select{
            width:200px;
            height:24px;
        }
        #passStrength{
            height:6px;width:120px;border:1px solid #ccc;padding:2px;
        }
        .strengthLv1{
            background:red;height:6px;width:40px;
        }
        .strengthLv2{
            background:orange;height:6px;width:80px;
        }
        .strengthLv3{
            background:green;height:6px;width:120px;
        }
        .code {
            font-family:Arial;
            font-style:italic;
            color:white;
            font-size:30px;
            border:0;
            padding:2px 3px;
            letter-spacing:3px;
            font-weight:bolder;
            float:left;
            cursor:pointer;
            width:150px;
            height:50px;
            line-height:60px;
            text-align:center;
            vertical-align:middle;
            background-color:#FF6699;
        }
        span {
            text-decoration:none;
            font-size:12px;
            color:#6699FF;
            padding-left:10px;
        }

        span:hover {
            text-decoration:underline;
            cursor:pointer;
        }
        #passStrength{
            height:6px;width:120px;border:1px solid #ccc;padding:2px;
        }
        .strengthLv1{
            background:red;height:6px;width:40px;
        }
        .strengthLv2{
            background:orange;height:6px;width:80px;
        }
        .strengthLv3{
            background:green;height:6px;width:120px;
        }
    </style>
    <script type="text/javascript">
        //生成验证码的方法
        function createCode(length) {
            var code = "";
            //验证码的长度
            var codeLength = parseInt(length);
            //验证码
            var checkCode = document.getElementById("checkCode");
            //所有候选组成验证码的字符，当然也可以用中文的
            var codeChars = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
                'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
                'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
            //循环组成验证码的字符串
            for (var i = 0; i < codeLength; i++) {
                //获取随机验证码下标
                var charNum = Math.floor(Math.random() * 62);
                //组合成指定字符验证码
                code += codeChars[charNum];
            }
            if (checkCode) {
                //为验证码区域添加样式名
                checkCode.className = "code";
                //将生成验证码赋值到显示区
                checkCode.innerHTML = code;
            }
        }

        //检查验证码是否正确
        function validateCode() {
            //获取显示区生成的验证码
            var checkCode = document.getElementById("checkCode").innerHTML;
            //获取输入的验证码
            var inputCode = document.getElementById("inputCode").value;
            //未输入验证码
            if (inputCode.length <= 0) {
                document.getElementById("verification").innerHTML="<font>请输入验证码！</font>";
            } else if (inputCode.toUpperCase() != checkCode.toUpperCase()) {
                document.getElementById("verification").innerHTML="<font>验证码错误！</font>";
                createCode(4);
            } else {
                document.getElementById("verification").innerHTML="<font class='right-color'>验证码正确！</font>";
            }
        }
    </script>

    <script type="text/javascript">
        /*添加事件监听兼容问题(为了机房中的IE浏览器做准备)处理*/
        EventListener={
            addEvent:function(ele,eve,fun){
                if(ele.addEventListener){
                    ele.addEventListener(eve,fun);
                }else if(ele.attachEvent){
                    ele.attachEvent("on"+eve,fun);
                }else{
                    ele["on"+eve]=fun;
                }
            }
        }
        window.onload=function(){
            createCode(4);
            var inputName=document.getElementsByTagName("input");
            var btnName=document.getElementsByClassName("button");
            var tdLeft=document.getElementsByClassName("td-left");
            var ID=selector("#ID");
            //第一次输入的密码
            var password=selector("#password");
            //第二次输入的密码
            var rpassword=selector("#Rpassword");
            var Email=selector("#E-mail");
            var checkName=selector("#checkName").getElementsByClassName("like");
            var form1=selector("#form1");

            /*ID验证*/
            EventListener.addEvent(inputName[0],"blur",function(){
                var UserFlag=checkUserAndCode(inputName[0].value);
                if(this.value==""){
                    ID.innerHTML="用户名不能为空！";
                }else if(inputName[0].value.length<3||inputName[0].value.length>18 ){
                    ID.innerHTML="用户名限制于3-18位！";
                }else if(UserFlag==false){
                    ID.innerHTML="大小写字母与数字并存！";
                }else{
                    ID.innerHTML="";
                }
            })
            EventListener.addEvent(inputName[0],"focus",function(){
                password.innerHTML="";
            })
            /*密码格式验证*/
            EventListener.addEvent(inputName[1],"blur",function(){
                var passwordFlag=checkUserAndCode(inputName[1].value);
                if(this.value==""){
                    password.innerHTML="密码不能为空！";
                }else if(inputName[1].value.length<6||inputName[1].value.length>8 ){
                    password.innerHTML="密码限制于6-8位！";
                }else if(passwordFlag==false){
                    password.innerHTML="大小写字母与数字并存！";
                }else{
                    password.innerHTML="";
                }
            })
            EventListener.addEvent(inputName[1],"focus",function(){
                password.innerHTML="";
            })
            /*确认密码验证*/
            EventListener.addEvent(inputName[2],"blur",function(){
                var flag=makeSuremima(inputName[1].value,inputName[2].value);
                if(flag==0){
                    rpassword.innerHTML="密码输入不能为空！";
                }else if(flag==1){
                    rpassword.innerHTML="";
                }else{
                    rpassword.innerHTML="两次密码输入不一致";
                }
            })
            EventListener.addEvent(inputName[2],"focus",function(){
                rpassword.innerHTML="";
            })
            /*邮箱格式验证*/
            EventListener.addEvent(inputName[7],"blur",function(){
                if(this.value==""){
                    Email.innerHTML="请填写您的邮箱！";
                }else if(isEmail(inputName[7])==false){
                    Email.innerHTML="邮箱格式错误！";
                }else{
                    Email.innerHTML="";
                }
            })
            EventListener.addEvent(inputName[7],"focus",function(){
                Email.innerHTML="";
            })
            /*提交按钮点击事件*/
            EventListener.addEvent(btnName[0],"click",function(){
                if(ID.innerHTML!=""||inputName[0].value==""||password.innerHTML!=""||inputName[1].value==""||rpassword.innerHTML!=""||inputName[2].value==""||Email.innerHTML!=""||inputName[7].value==""||selector("#verification").children[0].innerHTML!="验证码正确！"){
                    /*window.alert("请补全信息！");*/
                }
                else{
                    window.alert("注册成功！");
                    form1.submit();
                }
            })

            /*重置按钮之后不再显示提示内容*/
            EventListener.addEvent(btnName[1],"click",function(){
                window.location.reload();
            })

            /*密码是否相同*/
            function makeSuremima(obj1,obj2){
                obj1+="";
                obj2+="";

                var x=obj1.replace(/\s/g,"");
                var y=obj2.replace(/\s/g,"");

                if(x=="" || y==""){
                    return 0;
                }else if(x===y && x!="" && y!=""){
                    return 1;
                }else{
                    return 2;
                }
            }
            /*数字及大小写字母共存*/
            function checkUserAndCode(obj){
                var regA=/[A-Z]/;
                var rega=/[a-z]/;
                var regNum=/[0-9]/;

                var flag1=regA.test(obj);
                var flag2=rega.test(obj);
                var flag3=regNum.test(obj);

                if(flag1&&flag2&&flag3){
                    return true;
                }else{
                    return false;
                }
            }
            /*去除文本框内数据的所有空格(包含字符串中间的)*/
            function delSpace(obj){
                obj+="";
                obj=obj.replace(/\s/g,"");
                return obj;
            }
            /*获取select的选项值(调的是select下option的innerHTML)*/
            function selectOption(obj){
                var value="";
                for(var i=0;i<obj.length;i++){
                    if(obj[i].selected==true){
                        value=obj[i].innerHTML;
                    }
                }
                return value;
            }

            function selector(what){
                if(what.charAt(0)=="."){
                    return document.getElementsByClassName(what.slice(1));
                }else if(what.charAt(0)=="#"){
                    return document.getElementById(what.slice(1));
                }else{
                    return document.getElementsByTagName(what);
                }
            }
            /*邮箱格式验证*/
            function isEmail(obj)
            {
                var Email=/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                var Eflag=Email.test(obj.value);

                if(Eflag==true)
                {
                    return true;
                }else
                {
                    return false;
                }
            }
            /*获取checkBox的选项值*/
            function checkBox(checkName){
                var arr=[];
                for(var i=0;i<checkName.length;i++){
                    if(checkName[i].checked==true){
                        arr.push(checkName[i].value);
                    }
                }
                return arr;
            }
            /*重置时将所有warning消除,不再显示*/
            function noShow(arr){
                for(var i=0;i<arr.length-1;i++){
                    arr[i].innerHTML="";
                }
            }

        }
    </script>


</head>
<body>
<form method="post" id="form1" action="/userRegisterServlet">
    <table align="center">
        <tr>
            <th colspan="3">欢迎注册</th>
        </tr>
        <tr>
            <td colspan="3" height="20px"></td>
        </tr>
        <tr>
            <td class="td-left" >用户名</td>
            <td class="content"> <input name="username" type="text" class="text"/></td>
            <td id="ID" class="warning"></td>
        </tr>
        <tr>
            <td class="td-left">输入密码</td>
            <td class="content"> <input type="password" name="password" id="pass" maxlength="8"/> </td>
            <td id="password" class="warning"></td>
        </tr>
        <tr>
        </tr>
        <tr>
        <tr>
            <td class="td-left">密码强度</td>
            <td  colspan="2" height="40px"><div id="passStrength"></div></td>
        </tr>
        <tr>
            <td class="td-left">确认密码</td>
            <td class="content"><input type="password" name="rpass" class="text" maxlength="8"/></td>
            <td id="Rpassword" class="warning"></td>
        </tr>
        <tr>
            <td class="td-left">姓名</td>
            <td class="content">
                <input type="text" name="fullname" class="text"/>
            </td>
            <td id="LastName" class="warning">&nbsp;</td>
        </tr>
        <tr>
            <td class="td-left">手机号</td>
            <td class="content">
                <input type="text" name="phonenumber" class="text"/>
            </td>
            <td id="FirstName" class="warning">&nbsp;</td>
        </tr>
        <tr>
            <td class="td-left">住址</td>
            <td class="content">
                <input type="text" name="address" class="text"/>
            </td>
            <td id="address" class="warning">&nbsp;</td>
        </tr>
        <tr>
            <td>所在国家：</td>
            <td>
                <select id="country" name="country" style="width: 100px">
                        <option value ="中国">中国</option>
                        <option value ="美国">美国</option>
                        <option value="日本">日本</option>
                        <option value="英国">英国</option>
                        <option value="法国">法国</option>
                        <option value="意大利">意大利</option>
                        <option value="瑞士">瑞士</option>
                        <option value="泰国">泰国</option>
                        <option value="加拿大">加拿大</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="td-left">所在城市</td>
            <td class="content">
                <input id="city" type="text" name="city" style="width: 100px"/>
            </td>
            <td class="warning">&nbsp;</td>
        </tr>
        <tr>
            <td class="td-left">E-mail</td>
            <td class="content">
                <input type="text" name="email" class="text"/>
            </td>
            <td id="E-mail" class="warning">

            </td>
        </tr>
        <tr>
            <td>兴趣爱好</td>
            <td id="checkName">
                <input type="checkbox" name="hobby" class="like" value="Java开发"/>Java开发
                <input type="checkbox" name="hobby" class="like" value="Web前端开发"/>Web前端开发<br/>
                <input type="checkbox" name="hobby" class="like" value="Android开发"/>Android开发
                <input type="checkbox" name="hobby" class="like" value="UI设计"/>UI设计<br/>
                <input type="checkbox" name="hobby" class="like" value="系统运维"/>系统运维
            </td>
        </tr>
        <tr>
            <td>密保问题</td>
            <td>
                <select id="problem1" name="problem" style="width: 150px">
                    <option value="one" selected="selected">你最喜欢的节日</option>
                    <option value="two">你最喜欢的电影</option>
                    <option value="three">你最喜欢的音乐</option>
                    <option value="four">你的梦想是什么</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>密保答案</td>
            <td>
                <input type="text" name="secretanswer" class="text"/>
            </td>
        </tr>
        <tr>
            <td>验证码</td>
            <td class="content">
                <input type="text" id="inputCode" onblur="validateCode()" />
            </td>
            <td id="verification" class="warning">
            </td>
        </tr>
        <tr>
            <td>
                <div id="checkCode" class="code"  onclick="createCode(3)" >
                </div>
            </td>
            <td>
                <span onclick="createCode(4)">看不清换一张</span>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <div id="slideBar"></div>
            </td>
        </tr>
        <tr>
            <td colspan="2" height="30px"></td>
        </tr>
        <tr>
            <td align="right">
                <input type="submit" value="提交" class="button"/>
            </td>
            <td>
                <input type="reset" value="重置" class="button"/>
            </td>
        </tr>
    </table>
</form>
</body>
<script type="text/javascript">
    var dataList = ["0","1"];
    var options = {
        dataList: dataList,
        success:function(){
            console.log("show");
        },
        fail: function(){
            console.log("fail");
        }
    };
    SliderBar("slideBar", options);
</script>
<script type="text/javascript">
    function PasswordStrength(passwordID,strengthID){
        this.init(strengthID);
        var _this = this;
        document.getElementById(passwordID).onkeyup=function(){
            _this.checkStrength(this.value);
        }
    };
    PasswordStrength.prototype.init = function(strengthID){
        var id = document.getElementById(strengthID);
        var div = document.createElement('div');
        var strong = document.createElement('strong');
        this.oStrength = id.appendChild(div);
        this.oStrengthTxt = id.parentNode.appendChild(strong);
    };
    PasswordStrength.prototype.checkStrength = function (val){
        var aLvTxt = ['','低','中','高'];
        var lv = 0;
        if(val.match(/[a-z]/g)){lv++;}
        if(val.match(/[0-9]/g)){lv++;}
        if(val.match(/(.[^a-z0-9])/g)){lv++;}
        if(val.length < 6){lv=0;}
        if(lv > 3){lv=3;}
        this.oStrength.className = 'strengthLv' + lv;
        this.oStrengthTxt.innerHTML = aLvTxt[lv];
    };
</script>
<script type="text/javascript">
    new PasswordStrength('pass','passStrength');
</script>
</html>